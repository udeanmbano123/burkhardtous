﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace BurkhardTours.Models
{
    public class Contact
    {
    public string Names { get; set; }
    public string EmailAddress { get; set; }
    public string PhoneNumber { get; set; }
    public string subject { get; set; }
    public string comments { get; set; }
    }
}