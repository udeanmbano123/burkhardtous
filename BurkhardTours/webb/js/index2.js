var app = angular.module('ReviewForm', []);
app.controller("cfController", function ($scope, $http) {
    $scope.submitForm = function (isValid) {
        this.formInput = {
            name: $("input[name='name']").val(),
            email: $("input[name='email']").val(),
            message: $("textarea[name='message']").val()
        };
        $scope.data = {

            name: $("input[name='name']").val(),

            email: $("input[name='email']").val(),

            message: $("textarea[name='message']").val()

        };
        if (isValid) {

            var post = $http({
                method: "POST",
                url: "../Home/AddReview",
                dataType: 'json',
                data: { emp: $scope.data },
                headers: { "Content-Type": "application/json" }
            });

            post.success(function (data, status) {
                console.log(data);
            });
            console.log(this.formInput);
            $scope.data = {};
            this.formInput = {};
            $scope.cf.$setPristine();
            angular.element(document.getElementById("myModal")).modal();
        } else {
            alert('Failed to send message');
            console.log('Failed to send message');
        }
    };
});
app.controller('myController', function ($scope,$http,$interval) {
    $scope.dataList = [];
    $http.get('../Home/ReviewList').success(function (response) {
        if (response != null || response != "undefined") {
            $scope.dataList = response;
        }
    });
    $interval(function () {
        $http.get('../Home/ReviewList').success(function (response) {
        if (response != null || response != "undefined") {
            $scope.dataList = response;
        }
    });
},1000); // in milliseconds
});  