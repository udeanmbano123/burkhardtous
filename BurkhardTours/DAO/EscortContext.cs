﻿using BurkhardTours.Models;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Data.Entity.ModelConfiguration.Conventions;
using System.Linq;
using System.Web;


namespace BurkhardTours.DAO
{
    public class ChitadamaContext : DbContext
    {
        public ChitadamaContext()
            : base("ChitadamaContext")
        { }

        public DbSet<Review> reviews { get; set; }
  
        protected override void OnModelCreating(DbModelBuilder modelBuilder)
        {
            modelBuilder.Conventions.Remove<PluralizingTableNameConvention>();

         

            base.OnModelCreating(modelBuilder);
        }

     



        //  public System.Data.Entity.DbSet<CDSLogin.Models.ApplicationUser> ApplicationUsers { get; set; }

    }



}